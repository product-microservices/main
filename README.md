Sample of Microservices

It consists of three core microservices, the Product, Review, and Recommendation
services, all of which deal with one type of resource, and a composite microservice
called the Product Composite service, which aggregates information from the three
core services.
microservice landscape: The main goal of this project is up and running a set of microservices,
working together and putting some of the most common technologies in IT industry. Main 
programming language is java.

Source: Microservices with Spring Boot and Spring Cloud-2E
